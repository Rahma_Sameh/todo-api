from django.shortcuts import render
from rest_framework import generics, permissions, viewsets  # * new
from django.contrib.auth import get_user_model  # * new

from .models import Todo
from .serializers import TodoSerializer, UserSerializer   # * new
from .permissions import IsAuthorOrReadOnly

# Create your views here.
class ListTodo(generics.ListCreateAPIView):
    permissions_classes = (permissions.IsAuthenticated,)
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

class DetailTodo(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer 
    
    
class ListTodo(generics.ListCreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

class DetailTodo(generics.RetrieveUpdateDestroyAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
  

class TodoViewSet(viewsets.ModelViewSet):   # * new
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer