from django.urls import path 
from rest_framework.routers import SimpleRouter  # * new
from .views import ListTodo, DetailTodo, UserDetail, UserList   # * new
from .view import UserViewSet, TodoViewSet

router = SimpleRouter()
router.register('users', UserViewSet, basename='users')
router.register('', TodoViewSet, basename='todos')
urlpatterns = router.urls

urlpatterns = [
    path('users/', UserList.as_view()),   # * new
    path('users/<int:pk>/', UserDetail.as_view()),
    path('<int:pk>/', DetailTodo.as_view()),
    path('', ListTodo.as_view()),
]
